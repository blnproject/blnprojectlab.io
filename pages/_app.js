import React from 'react';
import App from 'next/app';

import { config } from '@fortawesome/fontawesome-svg-core'
import '@fortawesome/fontawesome-svg-core/styles.css' // Import the CSS
config.autoAddCss = false // Tell Font Awesome to skip adding the CSS automatically since it's being imported above

import { ThemeProvider } from 'styled-components'

const theme = {
  colors: {
    primary: '#E8BA3F',
    navColor: "#3CA2A2"
  },
}

import Loading from "../components/loading";

export default class MyApp extends App {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
  }

  componentDidMount() {
    setTimeout(()=>{
      this.setState({ isLoading: false })
    }, 1000)
  }

  render() {
    const {Component, pageProps} = this.props;
    const {isLoading} = this.state;
    return (
      <ThemeProvider theme={theme}>
        {isLoading ? <Loading /> :<Component {...pageProps} />}
      </ThemeProvider>
    )
  }
}