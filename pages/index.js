import Header from '../components/header';
import Footer from '../components/footer';
import Page from '../layouts/main';
import media from '../helpers/media';

import styled from 'styled-components';

import { AppleStore, GooglePlay, Apk } from '../components/button/download';

const FixedWrapper = styled.div`
  display: fixed;
  z-index: 1;
  width: 100%;
  height: 100%;
  position: relative;
  display: block;
`;

const FixedFooter = styled.div`
  position: absolute;
  bottom: 0;
  height: auto;
  width: 100%;
`;

const ContentWrapper = styled.div`
  padding: 50px 20px;
  @media ${media.medium} {
    padding: 100px 20px;
  }
`;

const H1 = styled.h1`
  text-align: center;
  width: 100%;
  color: #adadad;
  padding-buttom: 20px;
`;

const H3 = styled.h3`
  text-align: center;
  width: 100%;
  color: #adadad;
  padding-buttom: 20px;
`;

const SpanWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 30px 20px;
`;

const Span = styled.span`
  text-align: center;
  padding: 0 20px;
  color: ${props => props.theme.colors.primary};
`;

const ButtonWrapper = styled.div`
  text-align: center;
  color: ${props => props.theme.colors.primary};
  display: grid;
  justify-content: center;
  @media ${media.medium} {
    display: flex;
  }
`;

const Button = styled.span`
  background-color: black;
  margin: 10px 0;
  text-align:center;
  display: inline-block;
  width: 200px;
  border-radius: 5px;
  background-color: black;
  @media ${media.medium} {
    width: 150px;
    margin: 0 20px;
  }
`;


import Anime from "react-anime"

export default function Home() {
  return (
    <Page>
      <FixedWrapper>
        <Header />
        <ContentWrapper>
          <Anime opacity={[0, 1]} translateY={'1em'} delay={(e, i) => i * 500}>
            <H1>Blacknet</H1>
            <H3>Blacknet is extensible proof-of-stake network.</H3>
            <SpanWrapper>
              <Span>No Premine</Span>
              <Span>No ICO</Span>
              <Span>No Foundation</Span>
            </SpanWrapper>
            <ButtonWrapper>
              <Button><AppleStore /></Button>
              <Button><GooglePlay /></Button>
              <Button><Apk /></Button>
            </ButtonWrapper>
          </Anime>
        </ContentWrapper>
        <FixedFooter>
          <Footer />
        </FixedFooter>
      </FixedWrapper>
    </Page>
  )
}
