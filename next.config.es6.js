const optimizedImages = require('next-optimized-images');
const withPlugins = require('next-compose-plugins');
const withCSS = require('@zeit/next-css')
// module.exports = withCSS({
//   /* config options here */
// })
// const config = {
//   exportTrailingSlash: true
// };

module.exports = withPlugins([
  optimizedImages, {
    inlineImageLimit: 256
  }],
  withCSS({
    /* config options here */
  })
);
