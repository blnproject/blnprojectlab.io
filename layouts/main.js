import React from 'react';
import { ServerStyleSheet } from 'styled-components';
import Safe from 'react-safe';
import Head from 'next/head';

import faviconPNG from '../public/favicon.png';

const pageDesc = 'Blacknet is extensible proof-of-stake network.';
const pageTitle = 'Blacknet';

export default class MainLayout extends React.Component {
  static getInitialProps({ renderPage }) {
    const sheet = new ServerStyleSheet();
    const page = renderPage((App) => (props) =>
      sheet.collectStyles(<App {...props} />),
    );

    const styleTags = sheet.getStyleElement();
    return { ...page, styleTags };
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <Head>
          <base href="/" />
          <link rel="icon" type="image/png" href={faviconPNG} sizes="32x32" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width, minimal-ui"
          />
          <title>{pageTitle}</title>
          <meta name="og:title" content={pageTitle} />
          <meta name="description" content={pageDesc} />
          <meta name="og:description" content={pageDesc} />
          <meta name="mobile-web-app-capable" content="yes" />
          <meta name="fragment" content="!" />
          <Safe.script type="text/javascript" jsx="true" global="true">{`
            
          `}</Safe.script>
          {this.props.styleTags}
        </Head>
        <style jsx="true" global="true">{`
          * {
            font-family: 'Open Sans', sans-serif;
            box-sizing: border-box;
            margin: 0;
            padding: 0;
          }
          html {
            height: 100%;
          }
          body {
            height: 100%;
            width: 100%;
            background-color: #2D2D2D;
          }
          #__next {
            height: 100%;
            width: 100%;
          }
        `}</style>
        { this.props.children }
      </>
    );
  }
}
