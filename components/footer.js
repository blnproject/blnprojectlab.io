import styled from 'styled-components';
import { Gitlab } from './button/download';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faComments } from '@fortawesome/free-solid-svg-icons'
import { faBitcoin, faReddit } from '@fortawesome/free-brands-svg-icons'

const Wrapper = styled.div`
  display: block;
  width: 100%;
  text-align: center;
`;

const Span = styled.span`
  color: #969696;
  padding: 10px 0;
  font-size: 12px;
`;
const SpanImg = styled.span`
  display: inline-block;
  width: 30px;
  height: 28.61px;
`;

const CommunityWrapper = styled.div`
  display: flex;
  justify-content: center;
`;


const CommunityA = styled.a`
  color: ${props => props.theme.colors.primary};
  margin: 0 10px;
  font-size: 22px;
`;

const footer = () => {
  return (
    <Wrapper>
      <CommunityWrapper>
        <CommunityA href="https://app.element.io/#/room/#blacknet:matrix.org" target="_blank" title="Matrix room">
          <FontAwesomeIcon icon={faComments} />
        </CommunityA>
        <CommunityA href="https://bitcointalk.org/index.php?topic=469640.0" target="_blank" title="BitcoinTalk">
          <FontAwesomeIcon icon={faBitcoin} />
        </CommunityA>
        <CommunityA href="https://www.reddit.com/r/blacknet" target="_blank" title="Reddit">
          <FontAwesomeIcon icon={faReddit} />
        </CommunityA>
      </CommunityWrapper>
      <Span>
        The blockchain platform, © Blacknet Team
      </Span>
      <SpanImg>
        <Gitlab />
      </SpanImg>
    </Wrapper>
  );
};

export default footer;