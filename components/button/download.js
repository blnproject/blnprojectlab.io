import styled from 'styled-components';

import appstoreSVG from '../../public/images/app-store.svg';
import apkSVG from '../../public/images/apk.svg';
import googlePlaySVG from '../../public/images/google-play.svg';
import gitlabSVG from '../../public/images/gitlab-icon-rgb.svg';

const DownloadImg = styled.img`
  width: 100%;
  height: 100%;
  cursor: pointer;
  vertical-align:middle;
`;

const A = styled.a`
  cursor: pointer;
  text-decoration: none;
  display: inline-block;
  width: 120px;
  height: 44px;
  background-color: black;
`;

const A2 = styled.a`
  cursor: pointer;
  text-decoration: none;
  display: inline-block;
`;

export const GooglePlay = () => {
    return (
        <A target="_blank" href="https://play.google.com/store/apps/details?id=ninja.blacknet.wallet.blacknet">
            <DownloadImg src={googlePlaySVG} />
        </A>
    );
};

export const Apk = () => {
    return (
        <A target="_blank" href="https://gitlab.com/blacknet-ninja/blacknet-mobile/uploads/b4a23cc18d02c935c7dc220e3af10b8e/blacknet-mobile-3.0.2.apk">
            <DownloadImg src={apkSVG} />
        </A>
    );
};

export const Gitlab = () => {
    return (
        <A2 target="_blank" href="https://gitlab.com/blacknet-ninja">
            <DownloadImg src={gitlabSVG} />
        </A2>
    );
};

export const AppleStore = () => {
    return (
        <A target="_blank" href="https://apps.apple.com/us/app/blacknet/id1489451592">
            <DownloadImg src={appstoreSVG} />
        </A>
    );
};