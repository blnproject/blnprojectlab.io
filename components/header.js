import React, { useState } from 'react';
import styled from 'styled-components';
import Link from './active-link';

import media from '../helpers/media';

import logoPNG from '../public/logo.png';


const Wrapper = styled.div`
  display: flex;
  max-width: 1024px;
  margin: 0 auto;
  width: 100%;
`;

const HeaderWrapper = styled(Wrapper)`
  padding-top: 20px;
  align-items: center;
  @media ${media.medium} {
    padding-top: 30px;
  }
`;

const LogoWrapper = styled.div`
  cursor: pointer;
`;

// display: flex;
// justifyContent: center;
// alignItems: center;

const LogoImg = styled.img`
  width: 30px;
  cursor: pointer;
  vertical-align:middle;
  @media ${media.medium} {
    width: 40px;
  }
`;

const LogoTitle = styled.span`
  font-size: 20px;
  vertical-align:middle;
  padding-left: 5px;
  font-weight: bold;
  color: ${props => props.theme.colors.primary};
  @media ${media.medium} {
    font-size: 28px;
  }
`;

// nav
const Nav = styled.div`
  margin-left: auto;
  display: none;

  &.active {
    display: flex;
    flex-direction: column;
    width: 100%;
    height: auto;
    left: 0;
    top: 0;
    position: fixed;
    z-index: 100;
    background: #3B3B3B;
    padding: 30px 0 10px 0;
    margin: 0 auto;
    box-shadow: 0 15px 30px -10px rgba(0,0,0,.3);
    border: 1px solid #3B3B3B;
    z-index: 30;
  }

  @media ${media.medium} {
    display: flex;
  }
`;

const NavLink = styled.a`
  transition: color 0.2s ease-in-out;
  cursor: pointer;
  text-decoration: none;
  color: #adadad;
  line-height: 1.63;
  font-size: 22px;
  padding: 10px 40px;

  @media ${media.medium} {
    color: #adadad;
    line-height: 1.63;
    font-size: 16px;
    margin-left: 30px;
    border-bottom: solid 2px transparent;
    padding: 4px;
    text-align: center;

    &:hover {
      color: ${props => props.theme.colors.primary};
    }

    &.active {
      border-color: ${props => props.theme.colors.primary};
    }
  }
`;

const HamburgerIcon = styled.div`
  width: 19px;
  height: 22px;
  position: relative;
  transform: rotate(0deg);
  transition: .5s ease-in-out;
  margin: 8px 0 auto auto;
  display: block;
  z-index: 1001;
  

  @media ${media.medium} {
    display: none;
  }

  span {
    display: block;
    position: absolute;
    height: 2px;
    width: inherit;
    background: ${props => props.theme.colors.primary};
    border-radius: 8px;
    opacity: 1;
    left: 0;
    transform: rotate(0deg);
    transition: .25s ease-in-out;

    &:nth-child(1) {
      top: 0;
    }

    &:nth-child(2),
    &:nth-child(3) {
      top: 6px;
    }

    &:nth-child(4) {
      top: 12px;
      left: auto;
      right: 0;
      width: 13px;
    }
  }

  &.active {
    position: fixed;
    right: 20px;
    top: 20px;

    span {
      background: ${props => props.theme.colors.primary};

      &:nth-child(1) {
        top: 18px;
        width: 0%;
        left: 50%;
      }

      &:nth-child(2) {
        transform: rotate(45deg);
      }

      &:nth-child(3) {
        transform: rotate(-45deg);
      }

      &:nth-child(4) {
        top: 18px;
        width: 0%;
        left: 50%;
      }
    }
  }
}
`;

const MobileSocialIcons = styled.div`
  display: flex;
  padding: 0 40px;
  margin: auto 0 0 0;

  @media ${media.medium} {
    display: none;
  }
`;

const NavLinkMobile = styled(NavLink)`
  display: block;

  @media ${media.medium} {
    display: none;
  }
`;

const MobileBackground = styled.div`
  display: block;
  position: fixed;
  left: 0;
  top: 0;
  background: rgba(0, 0, 0, 0.75);
  height: 100vh;
  width: 100vw;
  z-index: 20;

  @media ${media.medium} {
    display: none;
  }
`;

export const Header = () => {
  const [showMenu, setShowMenu] = useState(false);
  const [navEnabled, setNavEnabled] = useState(false);

  function toggleNav() {
    document.body.style.overflow = navEnabled ? 'auto' : 'hidden';
    setNavEnabled(!navEnabled);
  }

  return (
    <HeaderWrapper>
      <Link href="/">
        <LogoWrapper>
          <LogoImg src={logoPNG} />
          <LogoTitle>Blacknet</LogoTitle>
        </LogoWrapper>
      </Link>
      {navEnabled && (
        <MobileBackground onClick={toggleNav} />
      )}
      <Nav className={navEnabled && 'active'}>
        <Link href="/" passHref>
          <NavLink>Home</NavLink>
        </Link>
        <NavLink target="_blank" href="/docs" >Docs</NavLink>
        <NavLink target="_blank" href="https://www.blnscan.io/" >Explorer</NavLink>
        <NavLink target="_blank" href="https://www.blnpool.io/" >Staking Pool</NavLink>
      </Nav>
      <HamburgerIcon
        onClick={toggleNav}
        className={navEnabled && 'active'}
      >
        <span />
        <span />
        <span />
        <span />
      </HamburgerIcon>
    </HeaderWrapper>
  );
};


const HeaderTheme1 = styled.div`
  width: 100%;
  position: relative;
  padding: 0 20px 25px 20px;
  @media ${media.medium} {
    padding: 0px;
  }
`;

export default () => {
  return (
    <HeaderTheme1>
      <Header>
      </Header>
    </HeaderTheme1>
  );
};