import React from 'react';
import styled from 'styled-components';
import PuffLoader from "react-spinners/PuffLoader";

const Center = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  left: 0;
  top: 0;
  overflow-y: auto;
  z-index: 10000;
  background-color: #2d2d2d;
`;

export default class Loading extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    document.body.style.position = 'fixed';
  }

  componentWillUnmount() {
    document.body.style.position = 'static';
  }

  handleClose() {
    this.props.onClose();
  }

  render() {
    return (
      <Center>
        <PuffLoader color={"#E8BA3F"} />
      </Center>
    );
  }
}


// import React from 'react';
// import { ActivityIndicator } from 'antd-mobile';

// interface LoadingProps {
//   text?: string;
//   size?: 'small' | 'large';
//   horizontal?: boolean;
// }

// const Loading: React.FC<LoadingProps> = props => {
//   const { text, size, horizontal } = props;
//   return (
//     <div
//       style={{
//         display: 'flex',
//         justifyContent: 'center',
//         alignItems: 'center',
//         height: '100%',
//       }}
//     >
//       <div
//         style={{
//           display: 'flex',
//           flexDirection: horizontal ? 'row' : 'column',
//           alignItems: 'center',
//           justifyItems: 'center',
//         }}
//       >
//         <ActivityIndicator size={size || 'large'} />
//         {text ? (
//           <span
//             style={{
//               marginTop: horizontal ? 0 : 8,
//               marginLeft: horizontal ? 8 : 0,
//             }}
//           >
//             {text}
//           </span>
//         ) : (
//           <></>
//         )}
//       </div>
//     </div>
//   );
// };

// export default Loading;
